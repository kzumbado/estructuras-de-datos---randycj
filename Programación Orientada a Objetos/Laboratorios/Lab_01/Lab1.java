/* Randy Conejo Juárez. 2019066448
 * Link para probar el código online: https://repl.it/@RandyCJ/prueba
 */



// Main.java

class Main {
  public static void main(String[] args){

    Cuenta C1 = new Cuenta(1122, 500000);
    C1.settasaDeInteresAnual(0.045);
    C1.depositarDinero(150000);
    C1.retirarDinero(200000);
    
    System.out.println("");
    System.out.println("");
    System.out.println("Enunciados 1 y 2 del Laboratorio 1 de POO");
    System.out.println("_______________________________________________");
    System.out.println("");
    System.out.println("Cuenta #1");
    System.out.println("Balance: " + 
                        + C1.getbalance() + "\n"
                        + "Interés Mensual: "
                        + C1.obtenerTasaDeInteresMensual() + "\n"
                        + "Fecha de Creación: "
                        + C1.getfechaDeCreacion());
  

    Cuenta C2 = new Cuenta(3477, 74000);
    C2.settasaDeInteresAnual(0.045);

    System.out.println("");
    System.out.println("Cuenta #2");
    System.out.println("Balance: " + 
                          + C2.getbalance() + "\n"
                          + "Interés Mensual: "
                          + C2.obtenerTasaDeInteresMensual() + "\n"
                          + "Fecha de Creación: "
                          + C2.getfechaDeCreacion());
  

    System.out.println("_______________________________________________");
    System.out.println("");
    System.out.println("");
    System.out.println("Lo siguiente corresponde al enunciado 3 del laboratorio");
    System.out.println("");
    
    ATM.sistema();
  }
  
  

// Cuenta.java

import java.util.Date;

public class Cuenta{
  
  // Declaración de los atributos
  private int id;
  private double balance;
  private double tasaDeInteresAnual;
  private Date fechaDeCreacion = new Date();

  // Constructor por defecto sin atributos
  public Cuenta(){
    this.id = 0;
    this.balance = 0;
    this.tasaDeInteresAnual = 0;

  }

  // Constructor con atributos
  public Cuenta(int id, double balance){
    this.id = id;
    this.balance = balance;
  }

  public int getid(){
    return this.id;
  }

  public double getbalance(){
    return this.balance;
  }

  public void setbalance(double balance){
    this.balance = balance;
  }

  public double gettasaDeInteresAnual(){
    return this.tasaDeInteresAnual;
  }

  public void settasaDeInteresAnual(double tasaDeInteresAnual){
    this.tasaDeInteresAnual = tasaDeInteresAnual;
  }

  public Date getfechaDeCreacion(){
    return this.fechaDeCreacion;
  }

  public double obtenerTasaDeInteresMensual(){
    return calcularInteresMensual();
  }

  public double calcularInteresMensual(){
    return (this.balance * this.tasaDeInteresAnual);
  }

  public void retirarDinero(double cantidad){
    this.balance = this.balance - cantidad;
  }

  public void depositarDinero(double cantidad){
    this.balance = this.balance + cantidad;
  }
}





//ATM.java

import java.util.Scanner;

public class ATM{

  public static void sistema(){
    Cuenta[] arregloCuentas = new Cuenta[10];

    for(int i=0; i<10; i++){
      arregloCuentas[i] = new Cuenta(i, 100000);
    }

    Scanner eleccion = new Scanner(System.in);
    int opcion;
    int id_ingresado;
    int dinero;

    while(true){

      System.out.println("Ingrese su ID");
      id_ingresado = eleccion.nextInt();
      System.out.println("");

      while (true){
        
        if (id_ingresado >9){
          System.out.println("El ID ingresado no existe");
          System.out.println("");
          System.out.println("Ingrese su ID");
          id_ingresado = eleccion.nextInt();
          System.out.println("");
          continue;
        }
        else if (id_ingresado <0){
          System.out.println("El ID ingresado no existe");
          System.out.println("");
          System.out.println("Ingrese su ID");
          id_ingresado = eleccion.nextInt();
          System.out.println("");
          continue;
        }
        else{System.out.println("El ID ingresado es correcto");
        System.out.println("");
              break;}
      }

      while (true){
        System.out.println(" _________________________");
        System.out.println("|                         |");
        System.out.println("|     Menú Principal      |");
        System.out.println("|1. Ver el balance actual |");
        System.out.println("|2. Retirar Dinero        |");
        System.out.println("|3. Depositar Dinero      |");
        System.out.println("|4. Salir                 |");
        System.out.println("|_________________________|");
        System.out.println("");
        System.out.println("Ingrese su selección.");
        System.out.println("");

        opcion = eleccion.nextInt();

        switch(opcion){

          case 1:

            System.out.println("El balance es de " + arregloCuentas[id_ingresado].getbalance());
            continue;
          
          case 2:

            System.out.println("Ingrese una cantidad para retirar:");
            dinero = eleccion.nextInt();
            arregloCuentas[id_ingresado].retirarDinero(dinero);
            continue;

          case 3:
            System.out.println("Ingrese una cantidad para depositar:");
            dinero = eleccion.nextInt();
            arregloCuentas[id_ingresado].depositarDinero(dinero);
            continue;

          case 4:
            System.out.println("");
            break;
        }
        System.out.println("");
        break;
      }
      continue;

    }
  }

}
