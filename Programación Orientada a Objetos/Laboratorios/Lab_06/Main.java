/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
class Main {
  public static void main(String[] args) {
    //Declaración del objeto controlador
    Controlador controlador = new Controlador();

    //Declaración del objeto casa
    Casa casa = new Casa();
    
    //Se declaran las habitaciones
    Habitacion cocina = new Habitacion("Cocina");
    Habitacion cuarto = new Habitacion("Cuarto");
    Habitacion salaEstar = new Habitacion("Sala de Estar");
    Habitacion salaLavado = new Habitacion("Sala de Lavado");

    //Se agregan a la casa las habitaciones
    casa.agregarHabitacion(cocina);
    casa.agregarHabitacion(cuarto);
    casa.agregarHabitacion(salaEstar);
    casa.agregarHabitacion(salaLavado);
 
    //Se crean y agregan los dispositivos correspondientes a la cocina
      
      //Crea los Objetos que pertenecen a cocina
      Microondas microondas = new Microondas();
      Refrigerador refrigerador = new Refrigerador();
      Estufa estufa = new Estufa();
      Extractor extractor = new Extractor();
      //Se agregan los dispositivos correspondientes a la cocina
      cocina.agregarDispositivo(microondas);
      cocina.agregarDispositivo(refrigerador);
      cocina.agregarDispositivo(estufa);
      cocina.agregarDispositivo(extractor);

      
    //Se crean y agregan los dispositivos correspondientes al cuarto
      
      //Crea los Objetos que pertenecen a cuarto
      Luces luces = new Luces();
      AireAcondicionado aireacondicionado = new AireAcondicionado();
      //Agrega los dispositivos correspondientes a cuarto
      cuarto.agregarDispositivo(luces);
      cuarto.agregarDispositivo(aireacondicionado);

    //Se crean y agregan los dispositivos correspondientes a salaEstar

      //Crea los Objetos que pertenecen a salaEstar
      TV tv = new TV();
      Abanico abanico = new Abanico();
      Lampara lampara = new Lampara();
      //Agrega los dispositivos correspondientes a salaEstar
      salaEstar.agregarDispositivo(tv);
      salaEstar.agregarDispositivo(abanico);
      salaEstar.agregarDispositivo(lampara);

  //Se crean y agregan los dispositivos correspondientes a salaLavado

    //Crea los Objetos que pertenecen a salaLavado
    Lavadora lavadora = new Lavadora();
    Secadora secadora = new Secadora();
    //Agrega los dispositivos correspondientes a salaLavado
    salaLavado.agregarDispositivo(lavadora);
    salaLavado.agregarDispositivo(secadora);

    
  //Pruebas del controlador
    System.out.println("\nEstos son los dispositivos de toda la casa y las habitaciones en donde se encuentran\n");
    controlador.imprimeObjetos(casa);

    System.out.print("\n\nRegresamos del trabajo y pues queremos cocinar: ");
    System.out.println("*Controlador enciende la cocina!!*\n");
    controlador.encender(cocina);
    System.out.println("*Actualiza el estado de los dispositivos*\n");
    controlador.imprimeObjetos(cocina);
    
    System.out.print("\n\nTerminamos nuestra cena: ");
    System.out.println("*Controlador apaga la cocina!!*\n");
    controlador.apagar(cocina);
    System.out.println("*Actualiza el estado de los dispositivos*\n");
    controlador.imprimeObjetos(cocina);

    System.out.print("\n\nEntonces deseamos relajarnos un rato: " );
    System.out.println("*Controlador enciende la TV!!*\n");
    controlador.encender(tv);
    System.out.println("*Actualiza el estado del dispositivo*\n");
    controlador.imprimeObjetos(salaEstar);

    System.out.print("\n\nTerminamos: ");
    System.out.println("*Controlador apaga la TV!!*\n");
    controlador.apagar(tv);
    System.out.println("*Actualiza el estado del dispositivo*\n");
    controlador.imprimeObjetos(salaEstar);

    System.out.print("\n\nEs hora de ir a descansar: ");
    System.out.println("*Controlador enciende Cuarto!!*\n");
    controlador.encender(cuarto);
    System.out.println("*Actualiza el estado del dispositivo*\n");
    controlador.imprimeObjetos(cuarto);

    System.out.print("\n\nUfff que frío: ");
    System.out.println("*Controlador cambia la temperatura del Aire!!*\n");
    aireacondicionado.cambiarTemp();
    System.out.println("*Actualiza el estado del dispositivo*\n");
    controlador.imprimeObjetos(cuarto);

    System.out.print("\n\nAhhhhh *bostezo* Buenas Noches: ");
    System.out.println("*Controlador apaga las Luces!!*\n");
    controlador.apagar(luces);
    System.out.println("*Actualiza el estado del dispositivo*\n");
    controlador.imprimeObjetos(cuarto);
  }
}