/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiantes: 
    -Kevin Zumbado Cruz (2019258634)
    -Randy Conejo Juarez (2019066448)
    
    Laboratorio 6
    Para probar este código en línea, visite el siguiente link: https://repl.it/@RandyCJ/Lab-06
**********************************************************************/
public class AireAcondicionado extends Dispositivo{
  private int temperatura;
  
  public AireAcondicionado(){
    super("Aire Acondicionado");
    this.temperatura=20;
  }

  public void cambiarTemp(){
    switch(temperatura){
      case 20:
        temperatura=25;
      break;

      case 25:
        temperatura=20;
      break;
    }
  }

  public String toString(){
    if (super.getEstado()=="Encendido"){
      return "Nombre: " + super.getAparato() + "\nEstado: " + super.getEstado()+"\nTemperatura: "+temperatura;  
    }else{
      return "Nombre: " + super.getAparato() + "\nEstado: " + super.getEstado(); 
    }
  }
}