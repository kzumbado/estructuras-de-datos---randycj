/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/

import java.util.ArrayList;

public class Oficial extends Persona{

  private String contrasena;
  private String usuario;
  private int placa;

  private ArrayList<Encargo> miEncargo;

  public Oficial(String usuario, String contrasena, String apellido, int placa){
    super(usuario, apellido);
    this.usuario=usuario;
    this.contrasena=contrasena;
    this.placa=placa;
    miEncargo = new ArrayList<Encargo>();
  }

  public String getUsuario(){
    return usuario;
  }
  
  public void AgregarEncargo(Encargo encargo){
    miEncargo.add(encargo);
  }
  
  public int getPlaca(){
    return placa;
  }
  public String getContrasena(){
    return contrasena;
  }

  public void verEncargo(){
    for (int i=0; i<miEncargo.size(); i++){
      System.out.println(miEncargo.get(i).toString());
    }
  }

  public String toString(){
    return "Nombre: "+ usuario +"\nApellido: "+ super.getApellido() + "\nPlaca: "+placa;
  }

}