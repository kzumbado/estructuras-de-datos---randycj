/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/

import java.util.Scanner;
import java.util.ArrayList;

class Main {

  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    Administrador admin = new Administrador();
    ArrayList<Persona> lista= new ArrayList<Persona>();
    lista.add(admin);

    int opc=0;
    int i;
    boolean inicio=false;
    do{
      System.out.println("\n1. Iniciar Sesión");
      System.out.println("0. Salir");
      opc=sc.nextInt();
      
      switch(opc){
        case 1:
          String user="";
          String pass="";
          System.out.print("\nUsuario: ");
          user=sc.next();
          System.out.print("Contraseña: ");
          pass=sc.next();

          for (i=0; i< lista.size(); i++){
            if (lista.get(i) instanceof Oficial){
              Oficial tmp=((Oficial)lista.get(i));
              if (tmp.getUsuario().equals(user) && tmp.getContrasena().equals(pass)){
                inicio=true;
              }
            }

            if (lista.get(i) instanceof Administrador){
              Administrador tmp=((Administrador)lista.get(i));
              if (tmp.getUsuario().equals(user) && tmp.getContrasena().equals(pass)){
                inicio=true;
              }
              
            }

            if (inicio){
              break;
            }
          }

          while (inicio){
            int op2=0;
            if (lista.get(i) instanceof Oficial){
                Oficial tmp=((Oficial)lista.get(i));
                System.out.println("\n1. Ver mis Encargos");
                System.out.println("0. Cerrar Sesión");
                op2=sc.nextInt();
                switch(op2){
                  case 1:
                    tmp.verEncargo();
                  break;

                  case 0:
                    inicio=false;
                  break;
                }
            }

            if (lista.get(i) instanceof Administrador){
                Administrador tmp=((Administrador)lista.get(i));
                System.out.println("\n1. Registrar nuevo Oficial");
                System.out.println("2. Ver Oficiales");
                System.out.println("3. Crear nuevo Encargo");
                System.out.println("4. Ver Encargos");
                System.out.println("5. Asignar Encargos");
                System.out.println("0. Cerrar Sesión");
                op2=sc.nextInt();
                switch(op2){
                  case 1:
                    System.out.println("\nIngrese el nombre");
                    String nmb=sc.next();
                    
                    System.out.println("\nIngrese el apellido");
                    String apl=sc.next();
                    
                    System.out.println("\nIngrese la contraseña");
                    String ctr=sc.next();

                    System.out.println("\nIngrese la placa");
                    int plc= sc.nextInt();

                    Oficial obj = new Oficial(nmb,ctr,apl,plc);
                    lista.add(obj);
                    tmp.registrarOficial(obj);

                    System.out.println("\n\nSe ha registrado correctamente");
                  break;

                  case 2:
                    tmp.verOficial();
                  break;

                  case 3:
                    System.out.println("\nIngrese el ID del encargo");
                    String id=sc.next();
                    
                    System.out.println("\nIngrese la descripción del encargo");
                    String dcp=sc.next();
                    Encargo encargo=new Encargo(id,dcp);

                    System.out.println("\nIngrese el plazo");
                    String plz=sc.next();
                    encargo.asignarPeriodo(plz);

                    tmp.crearEncargo(encargo);
                    System.out.println("\n\nSe ha creado correctamente");

                  break;

                  case 4:
                    tmp.verEncargo();
                  break;

                  case 5:
                    System.out.print("\nIngrese la placa del oficial: ");
                    plc=sc.nextInt();
                    
                    System.out.print("\nIngrese el ID del encargo: ");
                    id=sc.next();

                    for (int j=0; j<lista.size(); j++){
                      if (lista.get(j) instanceof Oficial){
                        Oficial oficial=((Oficial)lista.get(j));
                        if (oficial.getPlaca()==plc){
                          ArrayList<Encargo> listaE = new ArrayList<Encargo>();
                          listaE=tmp.getListaEncargo();
                          for(int k=0;k<listaE.size();k++){
                            if (listaE.get(k).getID().equals(id)){
                              lista.remove(j);
                              lista.add(j,tmp.asignarEncargo(oficial, listaE.get(k)));
                              
                              System.out.print("\nSe ha asignado correctamente\n");
                            }
                          }
                        }
                      }
                    }
                  break;

                  case 0:
                    inicio=false;
                  break;
                }
            }
          }


        break;
      }


    }while (opc!=0);
  
  }
}