/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/
public class Encargo implements Periodo{

  private String id;
  private String descripcion;
  private String plazo;

  public Encargo(String id,String descripcion){
    this.id=id;
    this.descripcion=descripcion;
  }

  @Override
  public void asignarPeriodo(String plazo){
    this.plazo=plazo;
  }
  
  public String getID(){
    return id;
  }

  public String toString(){
    return "\nID: "+id+"\nDescripción del Caso: "+descripcion+"\nPlazo: "+plazo;
  }

}