/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/

public abstract class Persona{
  private String nombre;
  private String apellido;

  protected Persona(String nombre, String apellido){
    this.nombre=nombre;
    this.apellido=apellido;
  }
  protected Persona(){
    this.nombre="Administrador";
    this.apellido="-";
  }

  public String getNombre(){
    return nombre;
  }
  
  public String getApellido(){
    return apellido;
  }
}