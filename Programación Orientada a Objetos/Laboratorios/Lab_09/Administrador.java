/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/

import java.util.ArrayList;

public class Administrador extends Persona{
  
  private String contrasena;
  private String usuario;

  private ArrayList<Encargo> listaEncargo;
  private ArrayList<Oficial> listaOficial;

  public Administrador(){
    super();
    this.usuario="admin";
    this.contrasena="admin";
    listaEncargo = new ArrayList<Encargo>();
    listaOficial = new ArrayList<Oficial>();
  }

  public Administrador(ArrayList<Oficial> listaOficial){
    super();
    this.usuario="admin";
    this.contrasena="admin";
    listaEncargo = new ArrayList<Encargo>();
    this.listaOficial = listaOficial;
  }

  public String getUsuario(){
    return usuario;
  }
  
  public String getContrasena(){
    return contrasena;
  }

  public void registrarOficial(Oficial objOficial){
    listaOficial.add(objOficial);
  }

  public void verOficial(){
    System.out.println("\n");
    for (int i=0; i<listaOficial.size(); i++){
      System.out.println(listaOficial.get(i).toString());
    }
  }

  public ArrayList<Encargo> getListaEncargo(){
    return listaEncargo;
  }

  public void verEncargo(){
    System.out.println("\n");
    for (int i=0; i<listaEncargo.size(); i++){
      System.out.println(listaEncargo.get(i).toString());
    }
  }

  public Oficial asignarEncargo(Oficial oficial, Encargo encargo){
    oficial.AgregarEncargo(encargo);
    return oficial;
  }

  public void crearEncargo(Encargo objEncargo){
    System.out.println("\n");
    listaEncargo.add(objEncargo);
  }

  public String toString(){
    return "Usuario: "+ usuario + "\nContraseña: "+contrasena;
  }
}

