/*********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos IC-2101
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    
    Estudiante: 
    -Kevin Zumbado Cruz (2019258634)
    
    Laboratorio 9
    Para probar este código en línea, visite el siguiente link: https://repl.it/@KevinZumbado/Lab09
**********************************************************************/

public abstract interface Periodo{
  public abstract void asignarPeriodo(String plazo);
}