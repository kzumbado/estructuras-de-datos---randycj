/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Proyecto Programado #1
    Estudiantes: Randy Conejo Juárez
                 Kevin Zumbado Cruz
                 Saúl Jiménez González
**********************************************************************/

//Declaración de Macros
#define RANGO 52 // de 1 a ..

//Declaración de estructuras
typedef struct Carta{
    int valor_total;
    int valor;
    struct Carta *sig;
    struct Carta *ant;
}Carta;

typedef struct CartaActual{
    Carta *cabeza;
    Carta *actual;
    int posicion_actual;
}CartaActual;

//Declaración de Funciones
/*-----------------------------------------------------------------------
    main
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void main();
/*-----------------------------------------------------------------------
    crear_cartas
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void crear_cartas();
/*-----------------------------------------------------------------------
    carta_nueva
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
Carta *carta_nueva(int valor_total, int valor);
/*-----------------------------------------------------------------------
    imprimir_cartas
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void imprimir_cartas();
/*-----------------------------------------------------------------------
    mostrar_actual
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void mostrar_actual(Carta *actual);
/*-----------------------------------------------------------------------
    carta_siguiente
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void carta_siguiente();
/*-----------------------------------------------------------------------
    carta_anterior
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void carta_anterior();
/*-----------------------------------------------------------------------
    barajar
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void barajar();
/*-----------------------------------------------------------------------
    menu
    Entradas:
    Salidas: 
    Funcionamiento:
        - 
-----------------------------------------------------------------------*/
void menu();
