#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "Proyecto_01.h"

//Declaración de estructuras globales a usar
CartaActual *carta_actual = NULL;

void main(){
    
    carta_actual = malloc(sizeof(CartaActual));
    crear_cartas();
    //imprimir_cartas();
    menu();
    
    return;
}


void crear_cartas(){
    int contador = 1;
    int contador_auxiliar = 1;
    Carta *temporal = NULL;
    Carta *nueva_carta = NULL;
    nueva_carta = malloc(sizeof(Carta));
    temporal = malloc(sizeof(Carta));
    
    while (contador <= 52){
    
        
        while (contador_auxiliar <= 13){//Se usan dos contadores, uno para el valor y otro para el valor total.
        
            nueva_carta = carta_nueva(contador, contador_auxiliar);//crea una variable de tipo carta
            if (contador == 1){
                temporal = nueva_carta;
                carta_actual->actual = nueva_carta;
                carta_actual->cabeza = nueva_carta;
                carta_actual->posicion_actual = 0;
            }
            else if (contador == 52){
                nueva_carta->sig=NULL;
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
            }
            else{
                temporal->sig = nueva_carta;
                nueva_carta->ant = temporal;
                temporal = nueva_carta;
            }
            contador ++;
            contador_auxiliar ++;
        }
    contador_auxiliar = 1;
    }
           
}

            
Carta *carta_nueva(int valor_total, int valor){
    Carta *nueva_carta = NULL;
    
    nueva_carta = malloc(sizeof(Carta));
    nueva_carta->valor_total = valor_total;
    nueva_carta->valor = valor;
    if (valor_total == 1){
        nueva_carta->ant = NULL;
    }
    return nueva_carta;
}
    
    
void imprimir_cartas(){
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    temporal = carta_actual->actual;
    
    while (temporal != NULL){
        printf("Valor : %d  Valor Total: %d\n", temporal->valor, temporal->valor_total);
        temporal = temporal->sig;
    }
    return;
}

void mostrar_actual(Carta *actual){

    int posicion = actual->valor_total;
    
    if (posicion <=13){
        if (actual->valor == 1){
            printf(".------.\n|A .   |\n| / \\  |\n|(_,_) |\n|  I  A|\n'------'\n");
        }
        else if(actual->valor == 10){
            printf(".------.\n|10.   |\n| / \\  |\n|(_,_) |\n|  I 10|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J .   |\n| / \\  |\n|(_,_) |\n|  I  J|\n'------'\n");
        }
        else if(actual->valor == 12){
            printf(".------.\n|Q .   |\n| / \\  |\n|(_,_) |\n|  I  Q|\n'------'\n");
        }
        else if(actual->valor == 13){
            printf(".------.\n|K .   |\n| / \\  |\n|(_,_) |\n|  I  K|\n'------'\n");
        }
        else {
            printf(".------.\n|%d .   |\n| / \\  |\n|(_,_) |\n|  I  %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
    
    else if(posicion <= 26){
        if (actual->valor == 1){
            printf(".------.\n|A_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ A|\n'------'\n");
        }
        else if(actual->valor == 10){
            printf(".-------.\n|10_  _ |\n| ( \\/ )|\n|  \\  / |\n|   \\/10|\n'-------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ J|\n'------'\n");
        }
        else if(actual->valor == 12){
            printf(".------.\n|Q_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ Q|\n'------'\n");
        }
        else if(actual->valor == 13){
            printf(".------.\n|K_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ K|\n'------'\n");
        }
        else {
            printf(".------.\n|%d_  _ |\n|( \\/ )|\n| \\  / |\n|  \\/ %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
        
    else if (posicion <= 39){
        if (actual->valor == 1){
            printf(".------.\n|A /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ A|\n'------'\n");
        }
        else if (actual->valor == 10){
            printf(".------.\n|10/\\  |\n| /  \\ |\n| \\  / |\n|  \\/10|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ J|\n'------'\n");
        }
        else if (actual->valor == 12){
            printf(".------.\n|Q /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ Q|\n'------'\n");
        }
        else if (actual->valor == 13){
            printf(".------.\n|K /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ K|\n'------'\n");
        }
        else {
            printf(".------.\n|%d /\\  |\n| /  \\ |\n| \\  / |\n|  \\/ %d|\n'------'\n", actual->valor, actual->valor);
        }
    }
        
    else {
        if (actual->valor == 1){
            printf(".------.\n|A _   |\n| ( )  |\n|(_x_) |\n|  Y  A|\n'------'\n");
        }
        else if (actual->valor == 10){
            printf(".------.\n|10_   |\n| ( )  |\n|(_x_) |\n|  Y 10|\n'------'\n");
        }
        else if (actual->valor == 11){
            printf(".------.\n|J _   |\n| ( )  |\n|(_x_) |\n|  Y  J|\n'------'\n");
        }
        else if (actual->valor == 12){
            printf(".------.\n|Q _   |\n| ( )  |\n|(_x_) |\n|  Y  Q|\n'------'\n");
        }
        else if (actual->valor == 13){
            printf(".------.\n|K _   |\n| ( )  |\n|(_x_) |\n|  Y  K|\n'------'\n");
        }
        else {
            printf(".------.\n|%d _   |\n| ( )  |\n|(_x_) |\n|  Y  %d|\n'------'\n", actual->valor, actual->valor);
        }
    }

    
    
    
}


void carta_siguiente(){
    carta_actual->actual = carta_actual->actual->sig;
    carta_actual->posicion_actual ++;
}

void carta_anterior(){
    carta_actual->actual = carta_actual->actual->ant;
    carta_actual->posicion_actual --;
}

void barajar(){
    int numero;
    int baraja[RANGO];
    int i = 0;
    int bandera;
    int familia;
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    srand(time(NULL));
    
    while (i<RANGO){
        bandera = 0;
        numero = (rand() % RANGO) + 1;
        if (i == 0){
            baraja[i] = numero;
            i ++;
        }
        else{
            for (int j=0; j<i; j++){
                if (baraja[j] != numero){
                    continue;
                }
                else{
                    bandera = 1;
                    break;
                }
            }
            if (bandera == 0){
                baraja[i] = numero;
                i ++;
            }
        }
    }    
    
    temporal = carta_actual->cabeza;
    
    for (int i=0; i<RANGO; i++){
        if (baraja[i] <= 13){
            familia = 0;
        }
        else if (baraja[i] <= 26){
            familia = 13;
        }
        else if (baraja[i] <= 39){
            familia = 26;
        }
        else {
            familia = 39;
        }
        
        temporal->valor_total = baraja[i];
        temporal->valor = baraja[i] - familia;
        temporal = temporal->sig;
    }

}

int sacarPivote(int *lista, int izq, int der)
{
    int i;
    int pivote, valor_pivote;
    int aux;

    pivote = izq;
    valor_pivote = lista[pivote];
    for (i=izq+1; i<=der; i++){
        if (lista[i] < valor_pivote){
                pivote++;
                aux=lista[i];
                lista[i]=lista[pivote];
                lista[pivote]=aux;

        }
    }
    aux=lista[izq];
    lista[izq]=lista[pivote];
    lista[pivote]=aux;
    return pivote;
}

void Quicksort(int *lista, int izq, int der)
{
     int pivote;
     if(izq < der){
        pivote=sacarPivote(lista, izq, der);
        Quicksort(lista, izq, pivote-1);
        Quicksort(lista, pivote+1, der);
     }
}
void ordenar(int *lista){
     
     int familia;
     
     Quicksort(lista,0,51);
     
     struct Carta *temporal = carta_actual->cabeza;
     
     for (int i=0; i<52; i++){
        if (lista[i] <= 13){
            familia = 0;
        }
        else if (lista[i] <= 26){
            familia = 13;
        }
        else if (lista[i] <= 39){
            familia = 26;
        }
        else {
            familia = 39;
        }
        
        temporal->valor_total = lista[i];
        temporal->valor = lista[i] - familia;
        temporal = temporal->sig;
    }    

}

void menu(){
    int eleccion;
    Carta *temporal = NULL;
    temporal = malloc(sizeof(Carta));
    int lista[52];
    int cont;
    do{
        printf("\n\t  Menú Principal\n");
	printf(" ________________________________\n");
	printf("|                                |\n");
        printf("| 1.\tMostrar Carta Actual     |\n");
        printf("| 2.\tMostrar Siguiente Carta  |\n");
        printf("| 3.\tMostrar Carta Anterior   |\n");
        printf("| 4.\tMostrar toda la baraja   |\n");
        printf("| 5.\tOrdenar Baraja           |\n");
        printf("| 6.\tBarajar                  |\n");
        printf("| 0.\tSalir                    |\n");
	printf(".________________________________.\n");
        printf("\nIngrese su elección: ");
        scanf("%d", &eleccion);
        printf("\n");
        
        switch (eleccion){
            
            case 1:
                mostrar_actual(carta_actual->actual);
                break;
            
            case 2:
                if (carta_actual->posicion_actual == 51){
                    printf("\nLlegó al final de la baraja\n");
                }
                else{
                    carta_siguiente();
                    mostrar_actual(carta_actual->actual);
                }
                break;
            
            case 3:
                if (carta_actual->posicion_actual == 0){
                    printf("\nLlegó al inicio de la baraja\n");
                }
                else{
                    carta_anterior();
                    mostrar_actual(carta_actual->actual);
                }
                break;
            
            case 4: 
                temporal = carta_actual->cabeza;
		printf("________________________________\n\n");
                while (temporal != NULL) {
                    mostrar_actual(temporal);
                    temporal = temporal->sig;
                }
		printf("\n________________________________\n");
                break;
	    case 5:
		temporal = carta_actual->cabeza;
		cont=0;
                while (temporal != NULL) {
		    lista[cont]=temporal->valor_total;
                    temporal = temporal->sig;
		    cont++;
                }
		ordenar(lista);
		printf("Se ha ordenado el mazo de cartas\n");
		break;
                    
            case 6:
                barajar();
                printf("Se ha barajado el mazo de cartas\n");
                break;

        }
    }while(eleccion != 0);
}
